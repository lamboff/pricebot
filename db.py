from sqlalchemy import create_engine, update
from sqlalchemy.pool import StaticPool
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey, Boolean, Date, Float
from sqlalchemy.orm import mapper
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import relationship


engine = create_engine("sqlite:///priceBotDB.db", connect_args={'check_same_thread': False}, poolclass=StaticPool,
                       echo=True)
metadata = MetaData()


class Product:
    def __init__(self, name, article, category, description=""):
        self.name = name
        self.article = article
        self.category = category
        self.description = description

    def __str__(self):
        return "{}({})- ".format(self.name, self.description)

    def __repr__(self):
        return self.__str__()


class ProductPriceItem:
    def __init__(self, product_id, price, shop_id, date):
        self.product_id = product_id
        self.price = price
        self.shop_id = shop_id
        self.date = date
        self.shop = self.get_shop()

    def __lt__(self, other):
        return self.price < other.price

    def __gt__(self, other):
        return self.price > other.price

    def get_shop(self):
            shop = session.query(Shop).filter_by(id=self.shop_id).one()
            return shop

    def __str__(self):
        return "{}({})".format(self.price, self.date)

    def __repr__(self):
        return self.__str__()


class FavoriteList:

    def __init__(self, user_id):
        self.user_id = user_id
        self.items_list = self.__get_items_by_user()

    def __get_items_by_user(self):
        self.items_list = session.query(FavoriteProductItem).filter_by(user_id=self.user_id).all()
        return self.items_list

    # def __str__(self):
    #     for i in self.items_list:
    #         i = session.query(Product).get(i)
    #         self.items_list.append(i)
    #     return "{}, {}".format(self.user_id, self.items_list)


class FavoriteProductItem:

    def __init__(self, product_id, user_id):
        self.product_id = product_id
        self.user_id = user_id

    def get_price_shop(self):
        prices = session.query(ProductPriceItem).filter_by(product_id=self.product_id).order_by('price').limit(3)
        shops = []
        for price in prices:
            query = "SELECT `name`, `address` FROM `shops` WHERE id=" \
                    "(SELECT shop_id FROM price_history WHERE product_id={} AND price={})".format(price.product_id, price.price)
            shops.append(str(session.execute(query).fetchall()))
        # price = item
        for i in shops:
            print(i)
        # shop = session.query(Shop).filter_by(id=item.shop_id).limit(3)
        # return price, shop

    @staticmethod
    def get_list_by_id(user_id):

        items = session.query(FavoriteProductItem.product_id).filter_by(user_id=user_id).all()
        list2show = []
        for i in items:
            list2show.append(session.query(Product).filter_by(id=i).first())
            print(list2show)

    def __str__(self):
        return '{}'.format(self.product_id)

    def __repr__(self):
        return self.__str__()


class User:
    def __init__(self, telegram_id, is_admin=False, is_stuff=False, telegram_name=None):
        self.telegram_id = telegram_id
        self.is_admin = is_admin
        self.telegram_name = telegram_name
        self.is_stuff = is_stuff

    def __str__(self):
        if self.telegram_name:
            return "{}".format(self.telegram_name)
        return "{}".format(self.telegram_id)

    def __repr__(self):
        return self.__str__()

    def is_admin(self):
        return self.is_admin

    def is_stuff(self):
        return self.is_stuff


class Shop:
    def __init__(self, name, address):
        self.name = name
        self.address = address
        self.rating = 0
    
    def get_rating(self, id):
        query = "SELECT rating FROM shop_rating WHERE shop_id = (SELECT id FROM shops WHERE shops.id = :name)"
        result = session.execute(query, {'name': id})
        score = []
        shop_id = ''
        for row in result:
            score.append(row['rating'])
            print(row)
        if len(score)>0:
            rating = sum(score)/len(score)
            query = "UPDATE shops SET rating=:rating WHERE id = :name"
            session.execute(query, {'rating': rating, "name": id})
            session.commit()
            self.rating = rating
            return round(self.rating, 2)
        else:
            self.rating = 0
    
    def __str__(self):
        return "{}({})".format(self.name, self.address)

    def __repr__(self):
        return self.__str__()


class Category:

    def __init__(self, cat_id):
        self.cat_id = cat_id
        self.name = self.__get_name_by_id

    def __get_name_by_id(self):
        if self.cat_id == 1:
            self.name = "Хлеб"
            return self.name
        elif self.cat_id == 2:
            self.name = "Молочна продукция"
            return self.name
        elif self.cat_id == 3:
            self.name = 'Мясная продукция'
            return self.name
        elif self.cat_id == 4:
            self.name = 'Не продовольственые товары'
            return self.name

    def __str__(self):
        return "{}".format(self.name)

    def __repr__(self):
        return self.__str__()


class CheckList:

    def __init__(self, user_id):
        self.user_id = user_id
        self.items_list = self.__get_items_by_user()

    def __get_items_by_user(self):
        self.items_list = session.query(CheckListItem).filter_by(user_id=self.user_id).all()
        return self.items_list

    # def __str__(self):
    #     for i in self.items_list:
    #         i = session.query(Product).get(i)
    #         self.items_list.append(i)
    #     return "{}, {}".format(self.user_id, self.items_list)

    def show_items_list(self):
        list2show = []
        for i in self.items_list:
            price , shop = i.get_best_price()
            list2show.append(str(session.query(Product).filter_by(id=i.product_id).first()) + "{}* {}".format(price, shop))
        return list2show


class CheckListItem:

    def __init__(self, product_id, user_id):
        self.product_id = product_id
        self.user_id = user_id

    def get_best_price(self):
        item = session.query(ProductPriceItem).filter_by(product_id=self.product_id).order_by('price').first()
        price = item.price
        shop = session.query(Shop).filter_by(id=item.shop_id).first()
        return price, shop

    def __str__(self):
        return '{}'.format(self.product_id)

    def __repr__(self):
        return self.__str__()


users_table = Table('users', metadata,
                    Column('telegram_id', Integer, primary_key=True),
                    Column('telegram_name', String(50), nullable=True),
                    Column('is_admin', Boolean, default=False),
                    Column('is_stuff', Boolean, default=False)
                    )


products_table = Table('products', metadata,
                       Column('id', Integer, primary_key=True),
                       Column('name', String(50)),
                       Column('article', Integer, unique=True),
                       Column('category', String(50)),
                       Column('description', String(150), nullable=True)
                       )


categories_table = Table('categories', metadata,
                         Column('cat_id', Integer, primary_key=True),
                         Column('name', String(50)))

price_history = Table('price_history', metadata,
                      Column('id', Integer, primary_key=True),
                      Column('product_id', ForeignKey('products.id')),
                      Column('date', Date),
                      Column('price', Float, nullable=False),
                      Column('shop_id', ForeignKey('shops.id'))
                      )


shops_table = Table('shops', metadata,
                    Column('id', Integer, primary_key=True),
                    Column('name', String(50)),
                    Column('address', String(50), unique=True),
                    Column('rating', Float, nullable=True),
                    # relationship('products.shop', backref='shops.name')
                    )


shop_rating_table = Table('shop_rating', metadata,
                          Column('shop_id', ForeignKey('shops.id')),
                          Column('rating', Float))

favorite_products = Table('favorite_products', metadata,
                          Column('id', Integer, primary_key=True),
                          Column('user_id', ForeignKey('users.telegram_id')),
                          Column('product_id', ForeignKey('products.id')))


check_list_items = Table('check_list_items', metadata,
                         Column('id', Integer, primary_key=True),
                         Column('user_id', ForeignKey('users.telegram_id')),
                         Column('product_id', ForeignKey('products.article')))


metadata.create_all(engine)
mapper(CheckListItem, check_list_items)
mapper(User, users_table, properties={
    'favorite': relationship(FavoriteProductItem, order_by=favorite_products.c.user_id),
    'list': relationship(CheckListItem, order_by=check_list_items.c.user_id)
})
mapper(FavoriteProductItem, favorite_products)
mapper(Product, products_table, properties={
    'prices':relationship(ProductPriceItem, order_by=price_history.c.price),

})
mapper(Shop, shops_table)
mapper(Category, categories_table)
mapper(ProductPriceItem, price_history, properties={
    'shop': relationship(Shop)
})
Session = sessionmaker()
Session.configure(bind=engine)
session = Session()


