import telebot
import os
from config import token, admin
from functions import *
from buttons import *
import requests

bot = telebot.TeleBot(token)


@bot.message_handler(commands=["start"])
def start_message(message):
    user = is_user(message.chat.id)
    if user:
        if user.is_admin:
            start_markup = admin_keyboard

        elif user.is_stuff:
            start_markup = stuff_keyboard

        else:
            start_markup = main_menu_btns

        if user.telegram_name:
            bot.send_message(message.chat.id, "Здравствуйте {}!".format(user.telegram_name),
                             reply_markup=start_markup)
        else:
            bot.send_message(user.telegram_id, "Здравствуйте!", reply_markup=start_markup)

    else:
        bot.send_message(message.chat.id, "Здравствуйте! Необоходима регистрация!".format(message.chat.username),
                         reply_markup=reg_btns)


@bot.message_handler(content_types=['document'])
def command_handle_document(message):
    file_path = bot.get_file(message.document.file_id)
    url = "https://api.telegram.org/file/bot{}/{}".format(token, file_path.file_path)
    r = requests.get(url)
    prods_list = r.text.split('\n')

    for item in prods_list[:len(prods_list)-1]:
        add_stuffs_to_db_from_txt(item.strip('/add '))
    bot.send_message(message.chat.id, 'Товары добавлены в базу')


@bot.message_handler(commands=["add"])
def new_product(message):
    try:
        user = is_user(message.chat.id)
        if not user.is_admin or user.is_stuff:
            new_prod = save_prod_from_user(message)
        else:
            new_prod = add_stuffs_to_db(message)
        if new_prod:
            bot.send_message(message.chat.id, "Товар успешно добавлен")
        else:
            bot.send_message(message.chat.id, "Товар добавлен для проверки Модератором!")
    except AttributeError as e:
        print(e)


@bot.callback_query_handler(func=lambda call: call.data == "stuff_check")
def send_file_to_check(call):
    file = open('prods.txt', 'r')
    bot.send_document(call.message.chat.id, file)
    file.close()
    os.remove('prods.txt')


@bot.callback_query_handler(func=lambda call: call.data == "check_list")
def show_check_list(call):
    check_list = CheckList(call.from_user.id)
    check_list = check_list.show_items_list()
    answer_msg = "\n\n".join(check_list)
    bot.send_message(call.message.chat.id, answer_msg)
    # bot.answer_callback_query(call.id, answer_msg, show_alert=True)


@bot.callback_query_handler(func=lambda call: call.data == "favorite_prod")
def show_check_list(call):
    favorite_list = FavoriteList(call.from_user.id)
    message_text = ''
    for item in favorite_list.items_list:
        prices = get_prices(item.product_id)
        message_text = ("# {}\n{}\n".format(get_product_by_id(item.product_id).name, get_product_by_id(item.product_id).category) + "".join(
            "{} - {}\n".format(item.get_shop(), item.price) for item in prices))

        # answer_msg = "\n\n".join(check_list)
    # bot.send_message(call.message.chat.id, answer_msg)
    bot.answer_callback_query(call.id, message_text, show_alert=True)


@bot.callback_query_handler(func=lambda call: call.data == "register")
def callback_inline(call):
    user = registration(call)
    bot.answer_callback_query(call.id, 'Вы зарегистрированы как {}'.format(user), show_alert=True)
    bot.send_message(admin, text="К нам присоединился {}".format(user))


@bot.callback_query_handler(func=lambda call: call.data.startswith('2checkList'))
def products_to_checklist(call):
    print(call)
    if get_or_create_products(session, CheckListItem, product_id=call.data.split()[1], user_id=call.from_user.id):
        bot.answer_callback_query(call.id, "Такой товар уже в корзине!", show_alert=True)
    else:
        bot.answer_callback_query(call.id, 'Товар добавлен в корзнину!!!')


@bot.callback_query_handler(func=lambda call: call.data.startswith('2favorite'))
def products_to_checklist(call):
    print(call)
    if get_or_create_products(session, FavoriteProductItem, product_id=call.data.split()[1], user_id=call.from_user.id):
        bot.answer_callback_query(call.id, "Товар  уже в любимых!", show_alert=True)
    else:
        bot.answer_callback_query(call.id, 'Товар добавлен в любимые!!!')


@bot.inline_handler(func=lambda query: len(query.query) > 0)
def query_text(query):

    if query.query.startswith('Prod '):
        try:
            keys = query.query.split(" ")[1]
            offset = int(query.offset) if query.offset else 0
            products = products_inline_query(keys)

            if len(products) is 0:
                try:
                    result = types.InlineQueryResultArticle(id='1',
                                                            title="Нет похожих товаров",
                                                            description=None,
                                                            input_message_content=types.InputTextMessageContent(
                                                                message_text="Такого товара пока нет в нашей базе"))
                    bot.answer_inline_query(query.id, [result])
                except Exception as e:
                    print(e)
            results_array = []
            try:
                m_next_offset = str(offset + 5) if len(products) == 5 else None

                for index, product in enumerate(products):
                    try:
                        results_array.append(types.InlineQueryResultArticle(id=str(offset + index), title=product.name,
                                                                            description="({})\n{}".
                                                                            format(product.category, product.description),

                                                                            input_message_content=
                                                                            types.InputTextMessageContent(
                                                        message_text=("# {}\n{}\n".format(product.name, product.category)
                                                                      + "".join("{}-{} руб.\n".format(item.shop, item.price)
                                                                      for item in product.prices[:3]))),
                                                                            reply_markup=prod_result_btns(product)))

                    except Exception as e:
                        print(e)
                # устанавливаем новый offset или сбрасываем, если в БД закончились релевантные записи
                bot.answer_inline_query(query.id, results_array, next_offset=m_next_offset if m_next_offset else "",)
            except Exception as e:
                print(e)
        except IndexError:
            pass

    elif query.query.startswith('Shop '):
        try:
            keys = query.query.split(" ")[1]
            offset = int(query.offset) if query.offset else 0
            shops = shops_inline_query(keys)
            if len(shops) is 0:
                try:
                    result = types.InlineQueryResultArticle(id='1', title="Извините, такого магазина пока нет в базе",
                                                            description=None,
                                                            input_message_content=types.InputTextMessageContent(
                                                                message_text="Извините, такого магазина пока нет в базе"))
                    bot.answer_inline_query(query.id, [result])
                except Exception as e:
                    print(e)
                return
            results_array = []
            try:
                m_next_offset = str(offset + 5) if len(shops) == 5 else None

                for index, shop in enumerate(shops):

                    try:
                        # При использовании подгрузки, ID должны быть уникальными в пределах всей большой пачки!
                        results_array.append(
                            types.InlineQueryResultArticle(id=str(offset + index), title=shop.name,
                                                           description="({})\n{}".format(shop.name, shop.address),
                                                           input_message_content=types.InputTextMessageContent(
                                                               message_text="# {0}\n {1}\n Рейтинг - {2}".format(
                                                                   shop.name, shop.address,(shop.get_rating(shop.id)))),
                                                           reply_markup=shop_result_btns(shop)))

                        print(results_array)
                    except Exception as e:
                        print(e)
                # устанавливаем новый offset или сбрасываем, если в БД закончились релевантные записи
                bot.answer_inline_query(query.id, results_array, next_offset=m_next_offset if m_next_offset else "")
            except Exception as e:
                print(e)
        except IndexError:
            pass


@bot.message_handler(content_types=["text"])
def show_info(message):
    if callback_inline:
        if message.text == "/menu":
            bot.send_message(message.chat.id, "Основное Меню!", reply_markup=main_menu_btns)
        elif message.text.startswith('#'):
            pass
        else:
            bot.send_message(message.chat.id, "Отправте /info или /menu")


if __name__ == '__main__':
    bot.polling(none_stop=True)


