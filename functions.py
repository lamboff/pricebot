import re
from datetime import datetime
from db import *

# User Block ==============================


def is_user(user_id):
    user = session.query(User).get(user_id)
    if user:
        return user
    return False


def registration(call):
    if call.message.chat.username:
        new_user = User(call.message.chat.id, is_admin=False, is_stuff=False, telegram_name=call.message.chat.username)
        session.add(new_user)
        session.commit()

        return new_user
    else:
        new_user = User(call.message.chat.id, is_admin=False, is_stuff=False)
        session.add(new_user)
        session.commit()

        return new_user


# End User Block ==============================

# Product Block ===============================

def get_or_create_products(session, model, article=None, **kwargs):
    instance = session.query(model).filter_by(article=article).first()
    if instance:
        return instance
    else:
        instance = model(article=article, **kwargs)
        session.add(instance)
        session.commit()
        instance = session.query(model).filter_by(**kwargs).first()
        return instance


def get_or_create_shops(session, model, **kwargs):
    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance
    else:
        instance = model(**kwargs)
        session.add(instance)
        session.commit()
        instance = session.query(model).filter_by(**kwargs).first()
        return instance


def get_product_by_id(product_id):
    product = session.query(Product).filter_by(id=product_id).first()
    return product


def add_stuffs_to_db(message):
    product_article = int(re.search(r'\d+', message.text.split('|')[0]).group(0))
    product_name = message.text.split('|')[1]
    product_price = float(message.text.split('|')[2])
    shop_name = re.search(r'\w+', message.text.split('|')[3])[0]
    shop_address = " ".join(message.text.split('|')[3].split()[1:]).strip('()')
    product_category = message.text.split('|')[4]
    try:
        product_description = message.text.split('|')[5]
    except IndexError:
        product_description = None

    current_date = datetime.now().date()

    shop = get_or_create_shops(session, Shop, name=shop_name, address=shop_address)
    category = get_or_create_shops(session, Category, cat_id=product_category)

    product_args = {
        'article': product_article,
        'name': product_name,
        'shop': str(shop),
        'price': product_price,
        'category': product_category,
    }
    product = get_or_create_products(session, Product, article=product_article, name=product_name, category=category.name, description=product_description)
    new_item_price = ProductPriceItem(product_id=product.id, price=product_price, shop_id=shop.id, date=current_date)
    session.add(new_item_price)
    session.commit()
    return product_args


def add_stuffs_to_db_from_txt(item):
    product_article = int(re.search(r'\d+', item.split('|')[0]).group(0))
    product_name = item.split('|')[1]
    product_price = float(item.split('|')[2])
    shop_name = re.search(r'\w+', item.split('|')[3])[0]
    shop_address = " ".join(item.split('|')[3].split()[1:]).strip('()')
    product_category = item.split('|')[4]
    try:
        product_description = item.split('|')[5]
    except IndexError:
        product_description = None

    current_date = datetime.now().date()

    shop = get_or_create_shops(session, Shop, name=shop_name, address=shop_address)
    category = get_or_create_shops(session, Category, cat_id=product_category)

    product_args = {
        'article': product_article,
        'name': product_name,
        'shop': str(shop),
        'price': product_price,
        'category': product_category,
    }
    product = get_or_create_products(session, Product, article=product_article, name=product_name, category=category.name, description=product_description)
    new_item_price = ProductPriceItem(product_id=product.id, price=product_price, shop_id=shop.id, date=current_date)
    session.add(new_item_price)
    session.commit()
    return product_args


def get_prices(product):
    prices = session.query(ProductPriceItem).filter_by(product_id=product).all()[0:3]
    prices = sorted(prices)
    return prices


def save_prod_from_user(message):
    with open('prods.txt', 'a') as f:
        f.write(message.text + "\n")
        f.close()

# Inline Queries Block ========================


def products_inline_query(keys):
    products = session.query(Product).filter(((Product.name).contains(keys))).all()
    return products


def shops_inline_query(keys):
    shops = session.query(Shop).filter(Shop.name.contains(keys)).all()
    return shops

#  END Inline Queries Block ========================
