from telebot import types

# Registration Buttons
reg_btns = types.InlineKeyboardMarkup(row_width=2)
registration_btn = types.InlineKeyboardButton(text='Запрос регистрации', callback_data='register')
cancel_btn = types.InlineKeyboardButton('Нет, спасибо!', callback_data='cancel')
reg_btns.add(registration_btn, cancel_btn)


# Main menu Users Buttons
main_menu_btns = types.InlineKeyboardMarkup(row_width=2)
products_btn = types.InlineKeyboardButton("Продукты", switch_inline_query_current_chat="Prod ")
shops_btn = types.InlineKeyboardButton("Магазины", switch_inline_query_current_chat="Shop ")
favorite_products_btn = types.InlineKeyboardButton('Избранные продукты', callback_data='favorite_prod')
check_list_btn = types.InlineKeyboardButton('Мой список', callback_data='check_list')
main_menu_btns.add(products_btn, shops_btn)
main_menu_btns.add(favorite_products_btn)
main_menu_btns.add(check_list_btn)


# Stuffs Buttons
stuff_keyboard = types.InlineKeyboardMarkup(row_width=2)
stuff_commands_btn = types.InlineKeyboardButton('Команды Модератора', callback_data='show_stuff_commands')
other_stuff = types.InlineKeyboardButton('Продукты на проверку', callback_data='stuff_check')
stuff_keyboard.add(stuff_commands_btn, other_stuff)


# Admins Buttons
admin_keyboard = types.InlineKeyboardMarkup(row_width=2)
admin_commands_btn = types.InlineKeyboardButton('Команды Админа', callback_data='show_admins_commands')
other_stuff = types.InlineKeyboardButton('Продукты на проверку', callback_data='stuff_check')
admin_keyboard.add(admin_commands_btn, other_stuff)


# Product Search Result Buttons
def prod_result_btns(product):
    product_result_btn = types.InlineKeyboardMarkup(row_width=2)
    to_check_list = types.InlineKeyboardButton('В корзину', callback_data='2checkList {}'.format(product.id))
    to_favorite_list = types.InlineKeyboardButton('В любимые', callback_data='2favorite {}'.format(product.id))
    product_result_btn.add(to_check_list, to_favorite_list)
    return product_result_btn


# Shop Search Result Buttons
def shop_result_btns(shop):
    kb = types.InlineKeyboardMarkup(row_width=2)
    to_favorite_list = types.InlineKeyboardButton('Отценить', callback_data='rating {}'.format(shop.id))
    kb.add(to_favorite_list)
    return kb